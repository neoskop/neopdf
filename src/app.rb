require 'cuba'
require 'digest'
require 'json'

def fetch_pdf url, parameters
  file_path = "/tmp/#{Digest::MD5.hexdigest url}.pdf"
  `wkhtmltopdf #{parameters} #{url} #{file_path}`
  file = Rack::File.new(nil)
  file.path = file_path
  halt(file.serving(env))
end

Cuba.define do
  on post do
    on root do
      begin
        res["Content-Type"] = "application/pdf"
        request_obj = JSON.parse(req.body.read)

        if !request_obj["url"].nil? and request_obj["passphrase"].eql? ENV["PASSPHRASE"]
          fetch_pdf(request_obj["url"], request_obj["parameters"])
        else
          res.status = 400
        end
      rescue JSON::ParserError
        res.status = 400
      end
    end
  end

  on get do
    on root do
      res.status = 200
    end

    on 'test' do
      fetch_pdf("https://www.neoskop.de", "")
    end
  end
end
