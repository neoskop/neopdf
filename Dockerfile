FROM ruby:2.2
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev tmpreaper
ENV TZ Europe/Berlin
ENV WKHTMLTOPDF_MAJOR_VERSION 0.12
ENV WKHTMLTOPDF_VERSION 0.12.3
COPY tmpreaper.conf /etc/tmpreaper.conf
COPY run.sh /run.sh
WORKDIR /usr/local
RUN wget -qO- http://download.gna.org/wkhtmltopdf/$WKHTMLTOPDF_MAJOR_VERSION/$WKHTMLTOPDF_VERSION/wkhtmltox-$WKHTMLTOPDF_VERSION\_linux-generic-amd64.tar.xz \
 | tar xvJ wkhtmltox --strip-components=1
COPY src /var/www/neopdf
WORKDIR /var/www/neopdf
RUN bundle install
CMD /run.sh
