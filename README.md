# neopdf

Der Service kann mit dem folgenden Befehl gestartet werden:

```bash
$ docker-compose start
```

Mit dem folgenden Befehl kann ein Dokument abgerufen werden:


```bash
$ curl -v --data "{\"url\":\"https://www.docker.com\", \"passphrase\":\"topsecret\", \"parameters\": \"--viewport-size 1280x768 --disable-external-links --enable-smart-shrinking --orientation portrait --print-media-type\"}" http://localhost:9292/ -o docker_com.pdf
```